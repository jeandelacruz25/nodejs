import moment from 'moment'

class Helper {
	timeServer () {
		return moment().format('HH:mm:ss')
	}
}

module.exports = Helper