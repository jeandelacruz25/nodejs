import len from 'object-length'
import socketio from 'socket.io'
import EnvironmentVariables from './environmentVariables'

let iosocket = ''

const env = new EnvironmentVariables()

class ioSockets {
	conectionSocketIO (server) {
		let ioAdapter = socketio.listen(server)
		return ioAdapter
	}

	setIOSocket (iosockets) {
		iosocket = iosockets
	}

	createRoom (socket, data) {
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		socket.join(nameRoom)
		console.log(`Creando Sala Contactabilidad -> ${nameRoom}`)
	}

	createRoomApi(socket, data) {
		const nameRoomApi = `roomContactabilidadApi:${data.nameRoomApi}`
		socket.join(nameRoomApi)
		console.log(`Creando Sala Contactabilidad Api -> ${nameRoomApi}`)
	}

	createRoomUser (socket, data) {
		const nameRoomUser = `roomContactabilidad:${data.nameRoom}:${data.userID}`
		socket.join(nameRoomUser)
		console.log(`Creando Sala Contactabilidad por Usuario -> ${nameRoomUser}`)
	}

	manageUsers (socket, data) {
		if(data.nameRoom !== undefined){
			const nameRoom = `roomContactabilidad:${data.nameRoom}`
			iosocket.to(nameRoom).emit('reloadManageUsers')
			console.log(`Actualizando lista de Usuarios -> ${nameRoom}`)
		}
	}

	reloadDatatable(socket, data){
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		iosocket.to(nameRoom).emit('reloadDatatable', { nameDatatable: data.nameDatatable })
		console.log(`Se actualiza datatable -> ${data.nameDatatable} -> ${nameRoom}`)
	}

	reloadApi(socket, data) {
		const nameRoomApi = `roomContactabilidad:${data.nameRoomApi}`
		iosocket.to(nameRoom).emit('reloadApi', { nameApi: data.nameApi })
		console.log(`Se actualiza funcion -> ${data.nameApi} -> ${nameRoom}`)
	}

	clearTimeOut(socket, data){
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		iosocket.to(nameRoom).emit('clearTimeOut', { nameTimeOut: data.nameTimeOut })
		console.log(`Se elimina timeout -> ${data.nameTimeOut} -> ${nameRoom}`)
	}

	reloadChat (socket, data) {
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		iosocket.to(nameRoom).emit('reloadMessageChat')
		console.log(`Actualizando la ventana de Chat -> ${nameRoom}`)
	}

	sendNotificationSMS (socket, data) {
		let arrayCliente = data.arrayCliente
		Object.keys(arrayCliente).forEach(function (index, item) {
			let arrayValue = arrayCliente[index]
			const nameCliente = (arrayValue.cliente.palabra_clave).toLowerCase()
			const avatarCliente = arrayValue.cliente.avatar
			const nameCampaign = arrayValue.campana === null ? arrayValue.id_bulk_sms : arrayValue.campana.nombre_campana
			const estadoSMS = arrayValue.estado_sms == 3 ? true : false
			const nameRoom = `roomContactabilidad:${nameCliente}`
			if(estadoSMS) {
				iosocket.to(nameRoom).emit('notificationSMS', { arrayCliente: arrayCliente[index] })
				console.log(`Se envia notificaciones al cliente ${nameCliente} con su campaña ${nameCampaign}`)
			}
		})
	}

	sendNotificationCampaign (socket, data) {
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		iosocket.to(nameRoom).emit('notificationCampaign', { nameCampaign: data.nameCampaign, tipoSubida: data.tipoSubida, licenseExceeded: data.licenseExceeded })
		console.log(`Se envia notificacion -> ${data.nameCampaign} -> Tipo Subidad ${data.tipoSubida} -> ${nameRoom}`)
	}

	validateSession(socket, data){
		const nameRoom = `roomContactabilidad:${data.nameRoom}:${data.userID}`
		iosocket.to(nameRoom).emit('validateSession')
		console.log(`Validacion de sesión -> ${nameRoom}`)
	}

	reloadMenuCliente(socket, data){
		const nameRoom = `roomContactabilidad:${data.nameRoom}`
		iosocket.to(nameRoom).emit('reloadMenuCliente')
		console.log(`Se actualiza menu cliente -> ${nameRoom}`)
	}

	reloadMenuUsuario(socket, data){
		const nameRoom = `roomContactabilidad:${data.nameRoom}:${data.userID}`
		iosocket.to(nameRoom).emit('reloadMenuUsuario')
		console.log(`Se actualiza menu usuario -> ${nameRoom}`)
	}
}

module.exports = ioSockets
