'use strict'
import dotenv from 'dotenv'

dotenv.config({path: './config/.env_production'})

class EnvironmentVariables {
	envExpress () {
		return {
			'expressPort': process.env.expressPort
		}
	}

	envNameRoom () {
		return {
			'nameRoom': process.env.nameRoom
		}
	}
}

module.exports = EnvironmentVariables