'use strict'
import http from 'http'
import https from 'https'
import express from 'express'
import ioSockets from './util/ioSockets'
import EnvironmentVariables from './util/environmentVariables'
import Helper from './util/helperScore'
import fs from 'fs'
import notifier from 'mail-notifier'

const env = new EnvironmentVariables()
const envExpress = env.envExpress()
const envNameRoom = env.envNameRoom()
const ioSocket = new ioSockets()
const helper = new Helper()
const si = require('systeminformation')

const app = express()
const server = http.createServer(app)
const expressPort = envExpress.expressPort
const nameRoom = envNameRoom.nameRoom

/*const imap = {
  user: "jdelacruz2712@gmail.com",
  password: "Etelvina12@@",
  host: "imap.gmail.com",
  port: 993, // imap port
  tls: true,// use secure connection
  tlsOptions: { rejectUnauthorized: false },
  markSeen: false
}*/

server.listen(expressPort, () => console.log(`El servidor Nodejs del Score esta escuchando el puerto : ${expressPort}`))

const ioAdapter = ioSocket.conectionSocketIO(server)

const iosocket = ioAdapter.sockets.on('connection', (socket) => {
	console.log(`Conexión establecida con el socketID: ${socket.id}`)

	socket.on('createRoom', data => {
		ioSocket.createRoom(socket, data)
		ioSocket.createRoomUser(socket, data)
		ioSocket.manageUsers(socket, data)
	})

	socket.on('createRoomApi', data => {
		ioSocket.createRoomApi(socket, data)
	})

	socket.on('disconnect', () => {
		console.log(`Se ha eliminado el socketID: ${socket.id}`)
		socket.disconnect(true)
	})

	socket.on('reloadManageUsers', data => {
		ioSocket.manageUsers(socket, data)
	})

	socket.on('sendMessageChat', () => {
		ioSocket.reloadChat(socket, data)
	})
	
	socket.on('notificationSMS', data => {
		ioSocket.sendNotificationSMS(socket, data)
	})

	socket.on('reloadDatatable', data => {
		ioSocket.reloadDatatable(socket, data)
	})

	socket.on('reloadApi', data => {
		ioSocket.reloadApi(socket, data)
	})

	socket.on('clearTimeOut', data => {
		ioSocket.clearTimeOut(socket, data)
	})

	socket.on('notificationCampaign', data => {
		ioSocket.sendNotificationCampaign(socket, data)
	})

	socket.on('validateSession', data => {
		ioSocket.validateSession(socket, data)
	})

	socket.on('reloadMenuCliente', data => {
		ioSocket.reloadMenuCliente(socket, data)
	})

	socket.on('reloadMenuUsuario', data => {
		ioSocket.reloadMenuUsuario(socket, data)
	})

})

/*notifier(imap)
.on('mail', function(mail){
	iosocket.emit('receivedMail', mail)
	console.log('Acabas de Recibir un nuevo correo')
}).start()*/

/*setInterval(function() {
	si.networkStats().then(data => {
	    console.log(data);
	})
}, 1000)*/

ioSocket.setIOSocket(iosocket)